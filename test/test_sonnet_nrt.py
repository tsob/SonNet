"""
Test functions in sonnet_nrt.py
"""

import numpy as np
import sonnet.sonnet_nrt as snrt

# -----------------------------------------------------------------------------
# The get_batch() function

def helper_get_batch(x, batch_size):
    y = None
    n_batches = 0
    for batch in snrt.get_batch(x, batch_size):
        n_batches += 1
        assert len(batch) <= batch_size, "got a batch with length > batch size"
        assert type(batch) == type(x), "type mismatch"

        if (type(batch) == np.ndarray):
            assert batch.dtype == x.dtype, "Numpy array dtypes don't agree"
            if y is None:
                y = batch
            else:
                y = np.append(y, batch, axis=0)
            if (len(batch.shape)>1):
                assert batch.shape[1:] == x.shape[1:]
        else:
            if y is None:
                y = batch
            else:
                y.extend(batch)

    # Check the batched and unbatched values are equal
    assert np.all(np.reshape(x, [-1]) == np.reshape(y, [-1])), \
           "Batched and unbatched values are not equal"

    # Chech the total number of batches we got
    expected_batches = np.ceil(len(x)/float(batch_size))
    assert expected_batches == n_batches, "Unexpected number of batches"

def test_get_batch():
    Xs = [
            np.random.uniform(size=(10)),
            np.random.uniform(size=(100)),
            np.random.uniform(size=(1000)).tolist(),
            np.random.uniform(size=(10,10)),
            np.random.uniform(size=(10,100)),
            np.random.uniform(size=(10,1000)),
            np.random.uniform(size=(100,10)),
            np.random.randint(2**32, size=(100,100)),
            np.random.uniform(size=(100,1000)),
            np.random.randint(2**32, size=(1000,10)),
            np.random.uniform(size=(1000,100)),
            np.random.uniform(size=(1000,1000)),
         ]
    batch_sizes = np.arange(1,101,3)

    for x in Xs:
        for batch_size in batch_sizes:
            helper_get_batch(x, batch_size)


