#!/usr/bin/env python2.7
"""
@name: sonnet_nrt.py
@desc: Sonifyiny Neural Nets with Python, Keras, Tensorflow, and more!
@auth: Tim O'Brien
@date: Octobre 5th, 2016
"""

# General
import os
import sys
import copy
import subprocess as sp
import numpy as np
import time
from distutils.dir_util import mkpath
import argparse
import logging
import yaml

# DSP
import scipy
from scipy.signal import fftconvolve
import librosa
from librosa.core import istft

# Plotting and image stuff
#  import matplotlib.pyplot as plt
#  import matplotlib.gridspec as gridspec
#  import matplotlib.image as mpimg
import cv2

# Noodle nets
#  import keras
#  from keras.preprocessing import image
from keras import backend as K

# If we want to plot the net architecture, we can include:
# from keras.utils.visualize_util import plot as keras_plot

# For pretrained models
from deep_learning_models.vgg16 import VGG16
from deep_learning_models.imagenet_utils import preprocess_input
#  from deep_learning_models.imagenet_utils import decode_predictions

# Time string to label runs
TIME_STRING = None


def main(input_video, config):
    """
    Main function
    """

    logger = logging.getLogger(__name__)
    logger.info("Starting SonNet!")

    if config["net"]["name"].lower() == 'vgg16':
        config["net"]["img_target_size"] = (224, 224)  # For VGG16 net
    else:
        raise NotImplementedError("Only VGG16 supported for now")

    # Load in the video
    try:
        x, img, frame_rate = load_and_preprocess_video(
                                  input_video,
                                  config["net"]["img_target_size"]
                                  )
    except Exception as e:
        logging.exception(e)
        raise e

    # Try to load activations from file
    activations = load_activations_from_file(
            input_video, config["output_paths"]["activations"])

    if activations is None:
        # Compute activations directly
        activations = compute_activations(x, config)

        # Save activations
        if "activations" in config["output_paths"]:
            save_activations_to_file(config["output_paths"]["activations"],
                                     activations,
                                     input_video
                                     )

    # Synthesize audio
    audio_file = synthesize_audio(
                    activations,
                    frame_rate,
                    sample_rate=config["sound"]["sample_rate"],
                    outputdir=config["output_paths"]["audio"]
                    )

    if "video" in config["output_paths"]:
        save_video_to_file(config["output_paths"]["video"],
                           video_file=input_video,
                           audio_file=audio_file
                           )

    if "configs" in config["output_paths"]:
        fname = os.path.join(
                config["output_paths"]["configs"],
                "{}.{}.yaml".format(
                    os.path.basename(sys.argv[0]),
                    TIME_STRING
                    )
                )
        with open(fname, 'w') as fh:
            yaml.dump(config, fh)


def compute_activations(x, config):
    """
    Compute the array of activations.
    """
    # Construct the model
    if config["net"]["name"].lower() == 'vgg16':
        model = VGG16(weights='imagenet')
        logger.debug("Loaded VGG-16 model")
    else:
        raise NotImplementedError("We only support VGG16 net for now.")

    # Function to extract late activations
    get_layer_output = K.function([model.layers[0].input],
                                  [model.layers[-2].output])

    # Initialize an empty activations matrix of the right shape
    n_activations = model.layers[-2].output_dim  # 4096
    n_frames = x.shape[0]  # Number of images in our input
    activations = np.empty((n_frames, n_activations))

    # Get activations (batched)
    i = 0
    for x_batch in get_batch(x, config["net"]["batch_size"]):
        # Get a batch of activations. Size should be
        # (batch_size, n_activations), but the last batch can be incomplete.
        act_batch = np.squeeze(
                np.array(get_layer_output([x_batch])),
                axis=0
                )

        # Copy them into the big array
        activations[i:min(i+config["net"]["batch_size"], n_frames),
                    :] = copy.copy(act_batch)

        # Increase index record-keeper
        i = min(i + config["net"]["batch_size"], n_frames)

        # Print a status report
        logger.debug("Processed {} frames".format(i))

    # We're done!
    logger.debug("Computed all activations, shape={}".format(
        activations.shape))

    # We could also get predictions via:
    # preds = model.predict(x)

    return activations


def get_batch(x, batch_size):
    """
    Generator for batching data, via axis 0.
    """
    for i in xrange(0, len(x), batch_size):
        yield x[i:i+batch_size]


def synthesize_audio(activations, frame_rate, outputdir,
                     sample_rate, channels=2):
    """
    Synthesize audio from activations
    """
    # TODO cleanup multichannel implementation
    n_frames, vec_length = activations.shape

    vec_length /= channels  # equal portions of the vec go to each channel

    #  win_length = n_fft = 2*(vec_length-1)
    win_length = 2*(vec_length-1)
    hop_length = win_length / 6
    window = scipy.signal.hanning

    kernel_width = 10
    kernel = np.logspace(-4.0, 0.0, kernel_width)[:, np.newaxis]
    kernel = kernel / np.sum(kernel)
    activations_convolved = fftconvolve(activations, kernel, mode='same')
    act_over_convolved = activations / np.maximum(activations_convolved, 0.01)

    # Warp activation strengths
    act_over_convolved = np.power(act_over_convolved, 1.5)

    if channels == 1:
        # stft_matrix = activations.astype(np.complex64).T
        stft_matrix = act_over_convolved.astype(np.complex64).T

        audio = istft(
                stft_matrix,
                hop_length=hop_length, win_length=win_length,
                window=window, center=True, dtype=np.float32
                )

        # Fade in audio at the beginning
        fade_samples = int(0.1 * sample_rate)  # 50 ms fade
        fade_mask = np.logspace(-4.0, 0.0, fade_samples)
        audio[:fade_samples] = audio[:fade_samples] * fade_mask

    elif channels == 2:
        #  print act_over_convolved.shape
        #  print vec_length

        stft_matrix_l = act_over_convolved[:, :vec_length].astype(
            np.complex64).T
        stft_matrix_r = act_over_convolved[:, vec_length:2*vec_length].astype(
            np.complex64).T

        audio_l = istft(
                    stft_matrix_l,
                    hop_length=hop_length, win_length=win_length,
                    window=window, center=True, dtype=np.float32
                    )
        audio_r = istft(
                    stft_matrix_r,
                    hop_length=hop_length, win_length=win_length,
                    window=window, center=True, dtype=np.float32
                    )

        #  print audio_l.shape, audio_r.shape

        # Fade in audio at the beginning
        fade_samples = int(0.1 * sample_rate)  # 50 ms fade
        fade_mask = np.logspace(-4.0, 0.0, fade_samples)  # -60 dB to 0 dB

        audio_l[:fade_samples] = audio_l[:fade_samples] * fade_mask
        audio_r[:fade_samples] = audio_r[:fade_samples] * fade_mask
        audio = np.hstack((audio_l[:, np.newaxis], audio_r[:, np.newaxis]))

    # Resample the audio, since hop and frame rates are different
    vec_period = hop_length / float(sample_rate)
    frame_period = 1/float(frame_rate)
    resample_factor = frame_period / vec_period
    num_resampled = int(round(resample_factor*len(audio)))

    audio = scipy.signal.resample(audio, num_resampled, window='hamming')

    if outputdir is not None:
        # Write audio to file
        fname = os.path.join(
                outputdir,
                "{}.{}.wav".format(
                    os.path.basename(sys.argv[0]),
                    TIME_STRING
                    )
                )
        librosa.output.write_wav(fname, audio.T, sample_rate)
        # Return the filename
        return fname
    else:
        # Return the audio
        return audio


def load_activations_from_file(video_path, activations_dir):
    """
    Check if a suitable activations file already exists, and load it if so.
    Otherwise, return None.
    """
    logger = logging.getLogger(__name__)
    logger.info("Attempting to find suitable, pre-computed activations.")

    match = None
    prefix = os.path.basename(video_path).replace('.', '_')
    for candidate in os.listdir(activations_dir):
        if candidate[:len(prefix)] == prefix:
            candidate_ctime = os.path.getctime(os.path.join(
                                activations_dir, candidate))
            try:
                match_ctime = os.path.getctime(os.path.join(
                                    activations_dir, match))
            except:
                match_ctime = float('-inf')

            if (match is None) or (candidate_ctime > match_ctime):
                match = candidate

    if match is not None:
        logger.info("Loading activations from {}".format(
                                        os.path.join(activations_dir, match)))
        # Load the activations
        activations = np.genfromtxt(
                os.path.join(activations_dir, match),
                delimiter=','
                )
        return activations
    else:
        logger.info("Could not find suitable, pre-computed activations.")
        return match


def save_activations_to_file(outputdir, activations, video_path):
    """
    Write activations to csv file.
    """
    logger = logging.getLogger(__name__)

    fname = os.path.join(
            outputdir,
            "{}.{}.{}.activations.csv".format(
                os.path.basename(video_path).replace('.', '_'),
                os.path.basename(sys.argv[0]),
                TIME_STRING
                )
            )
    np.savetxt(fname, activations, delimiter=',')
    logger.info("Wrote activations to {}".format(fname))


def save_video_to_file(outputdir, video_file, audio_file):
    """
    Write combined video/sonified sound to wav file.
    """
    fname = os.path.join(
            outputdir,
            "{}.{}.activations{}".format(
                os.path.basename(sys.argv[0]),
                TIME_STRING,
                os.path.splitext(video_file)[1]
                )
            )
    try:
        sp.check_call(["ffmpeg", "-i", video_file, "-i", audio_file,
                       "-map", "0:v", "-map", "1:a",
                       "-c:a", "libvorbis", "-shortest", fname])
    except sp.CalledProcessError as e:
        logger.error("Type: {}, Args: {}, Message: {}".format(
            type(e), e.args, e.message))

    logger.info("Wrote video to {}".format(fname))


def save_config_to_file(outputdir, config):
    """
    Write config to file.
    """
    raise NotImplementedError()


def load_and_preprocess_video(video_path, img_target_size):
    """
    Load a video and preprocess each frame as successive images, for input to
    the net.
    """

    # Setup logger, as usual
    logger = logging.getLogger(__name__)
    logger.info("Loading and preprocessing video from {}".format(video_path))

    # Find OpenCV major version
    cv2_major_ver = (cv2.__version__).split('.')[0]

    # Open the video stream from file
    vidcap = cv2.VideoCapture(video_path)

    # Get frame rate, in frames per second
    if int(cv2_major_ver) < 3:
        frame_rate = vidcap.get(cv2.cv.CV_CAP_PROP_FPS)
    else:
        frame_rate = vidcap.get(cv2.CAP_PROP_FPS)
    logger.debug("Frame rate: {}".format(frame_rate))

    # Read the first frame
    success, vidframe = vidcap.read()
    input_shape = vidframe.shape
    logger.debug("Input frame shape: {}".format(input_shape))

    # Note that cv2 imports as BGR, not RGB. Change that.
    vidframe = cv2.cvtColor(vidframe, cv2.COLOR_BGR2RGB)
    img = np.asarray(copy.copy(vidframe))
    img = np.expand_dims(img, axis=0)

    # Resize
    vidframe = cv2.resize(vidframe, img_target_size,
                          interpolation=cv2.INTER_AREA)

    input_image = np.empty(
        shape=(1, img_target_size[0], img_target_size[1], 3)
        )
    input_image[0] = vidframe

    count = 0
    success = True
    while success:
        success, vidframe = vidcap.read()
        count += 1
        # Resize
        if vidframe is not None:
            # Convert to RGB
            vidframe = cv2.cvtColor(vidframe, cv2.COLOR_BGR2RGB)
            img = np.append(
                    img,
                    np.expand_dims(np.asarray(copy.copy(vidframe)), axis=0),
                    axis=0
                    )
            # Resize
            vidframe = cv2.resize(vidframe, img_target_size)
            # Add first dim for batch
            vidframe = np.expand_dims(vidframe, axis=0)
            # Append to batch
            input_image = np.append(input_image, vidframe, axis=0)

    logger.debug("Loaded {} frames".format(count))

    # Preprocess, for input into the net
    x = preprocess_input(input_image)

    return x, img, frame_rate


def parse_logging_level(level_arg):
    """
    Set the logging level based on the verbosity argument.
    """
    if level_arg == 0:
        logging_level = logging.WARNING
    elif level_arg == 1:
        logging_level = logging.INFO
    elif level_arg == 2:
        logging_level = logging.DEBUG
    else:
        logging_level = logging.DEBUG

    return logging_level


def maybe_create_dirs(dirname):
    """
    like mkdir -p
    """
    mkpath(dirname)


def parse_config(config_file):
    """
    Parse the given config YAML file.

    Argument:
        config_file: the path to the config YAML file to use.

    Returns:
        config: the config dictionary.
    """

    with open(config_file, 'r') as fh:
        config = yaml.load(fh)

    return config


if __name__ == '__main__':
    # Commandline argument handling
    parser = argparse.ArgumentParser(
            description="SonNet, non-real-time implementation."
            )
    parser.add_argument("-c", "--config",
                        required=False,
                        default='config/config.template.yaml',
                        help="Path to the config yaml file to use.")
    args = parser.parse_args()

    config = parse_config(args.config)
    # Record the original config file name
    config["original_filename"] = args.config

    # Create output directories if necessary
    for key in config["output_paths"]:
        maybe_create_dirs(config["output_paths"][key])

    # Parse input files
    if type(config["input"]["video"]) is str:
        # Only a single video file
        config["input"]["video"] = [config["input"]["video"]]

    videos = config["input"]["video"]
    for input_video in videos:
        # Process each video sequentially

        # Record this current video for the saved config
        config["input"]["video"] = input_video

        # Get unique timestamp for this run/video
        TIME_STRING = time.ctime().replace(' ', '_').replace(':', '-')

        # Setup logger
        logging_level = parse_logging_level(config["verbosity"])
        if config["output_paths"]["logs"] is None:
            # Don't output log to file, only stdout
            logging.basicConfig(
                    level=logging_level,
                    format='[%(levelname)s] %(message)s',
                    )
        else:
            # Output log to specified folder
            logfile = os.path.abspath(
                        os.path.join(
                            config["output_paths"]["logs"],
                            "{}.{}.log".format(
                                os.path.basename(sys.argv[0]),
                                TIME_STRING
                                )
                            )
                        )

            logging.basicConfig(
                    level=logging_level,
                    format='[%(asctime)s] [%(levelname)s] %(message)s',
                    filename=logfile
                    )

            # Add a handler so we output to stderr as well
            logging.getLogger().addHandler(logging.StreamHandler())

        logger = logging.getLogger(__name__)

        # Print output directories
        for key in config["output_paths"]:
            logger.info("Saving {} to {}.".format(
                key, config["output_paths"][key]))

        # Start the show
        main(input_video, config)
