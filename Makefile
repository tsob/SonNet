update:
	git submodule update --init --recursive # Add submodule
	touch deep_learning_models/__init__.py  # Make sure submodule is a py pkg

caltech-101:
	wget http://www.vision.caltech.edu/Image_Datasets/Caltech101/101_ObjectCategories.tar.gz
	mkdir -p images/Caltech101/
	tar -xzf 101_ObjectCategories.tar.gz -C images/Caltech101/
	rm 101_ObjectCategories.tar.gz

test_1:
	mkdir -p images/test_1/
	ln -s `pwd`/images/Caltech101/101_ObjectCategories/wild_cat/image_0027.jpg  images/test_1/img_0001.jpg
	ln -s `pwd`/images/Caltech101/101_ObjectCategories/headphone/image_0038.jpg images/test_1/img_0002.jpg
	ln -s `pwd`/images/Caltech101/101_ObjectCategories/platypus/image_0008.jpg  images/test_1/img_0003.jpg
	ln -s `pwd`/images/Caltech101/101_ObjectCategories/mandolin/image_0002.jpg  images/test_1/img_0004.jpg
	ln -s `pwd`/images/Caltech101/101_ObjectCategories/brain/image_0069.jpg     images/test_1/img_0005.jpg
	ffmpeg -framerate 1 -pattern_type glob -i 'images/test_1/*.jpg' -c:v libx264 images/test_1/out.mp4

ia-videos:
	-wget -nc https://archive.org/download/SlowMotionFlame/slomoflame_512kb.mp4 -O videos/slomoflame.mp4
	-wget -nc https://archive.org/download/CEP146/CEP146_512kb.mp4 -O videos/edison_cowboy.mp4
	-wget -nc https://archive.org/download/CEP_00_032/CEP_00_032_512kb.mp4 -O videos/hbomb_test.mp4

ia-nasa-ogv:
	-wget -nc https://ia600205.us.archive.org/12/items/HSF-mov-apollo11_launchclip02/apollo11_launchclip02.ogv -O videos/apollo11_launchclip02.ogv
	-wget -nc https://ia800206.us.archive.org/16/items/HSF-mov-apollo11_reclip11/apollo11_reclip11.ogv -O videos/apollo11_reclip11.ogv
	-wget -nc https://archive.org/download/HSF-mov-apollo11_reclip04/apollo11_reclip04.ogv -O videos/apollo11_reclip04.ogv
	-wget -nc https://ia600204.us.archive.org/4/items/HSF-mov-apollo11_onbclip01/apollo11_onbclip01.ogv -O videos/onbclip01.ogv
	-wget -nc https://ia600204.us.archive.org/4/items/HSF-mov-apollo11_onbclip02/apollo11_onbclip02.ogv -O videos/onbclip02.ogv
	-wget -nc https://ia600204.us.archive.org/4/items/HSF-mov-apollo11_onbclip03/apollo11_onbclip03.ogv -O videos/onbclip03.ogv
	-wget -nc https://ia600204.us.archive.org/4/items/HSF-mov-apollo11_onbclip04/apollo11_onbclip04.ogv -O videos/onbclip04.ogv
	-wget -nc https://ia600204.us.archive.org/4/items/HSF-mov-apollo11_onbclip05/apollo11_onbclip05.ogv -O videos/onbclip05.ogv
	-wget -nc https://ia600204.us.archive.org/4/items/HSF-mov-apollo11_onbclip06/apollo11_onbclip06.ogv -O videos/onbclip06.ogv
	-wget -nc https://ia600204.us.archive.org/4/items/HSF-mov-apollo11_onbclip07/apollo11_onbclip07.ogv -O videos/onbclip07.ogv
	-wget -nc https://ia600204.us.archive.org/4/items/HSF-mov-apollo11_onbclip08/apollo11_onbclip08.ogv -O videos/onbclip08.ogv
	-wget -nc https://ia600204.us.archive.org/4/items/HSF-mov-apollo11_onbclip09/apollo11_onbclip09.ogv -O videos/onbclip09.ogv
	-wget -nc https://ia600204.us.archive.org/4/items/HSF-mov-apollo11_onbclip10/apollo11_onbclip10.ogv -O videos/onbclip10.ogv
	-wget -nc https://ia600204.us.archive.org/4/items/HSF-mov-apollo11_onbclip11/apollo11_onbclip11.ogv -O videos/onbclip11.ogv
	-wget -nc https://ia600204.us.archive.org/4/items/HSF-mov-apollo11_onbclip12/apollo11_onbclip12.ogv -O videos/onbclip12.ogv
	-wget -nc https://ia600204.us.archive.org/4/items/HSF-mov-apollo11_onbclip13/apollo11_onbclip13.ogv -O videos/onbclip13.ogv
	-wget -nc https://ia600204.us.archive.org/4/items/HSF-mov-apollo11_onbclip14/apollo11_onbclip14.ogv -O videos/onbclip14.ogv
	-wget -nc https://ia600204.us.archive.org/4/items/HSF-mov-apollo11_onbclip15/apollo11_onbclip15.ogv -O videos/onbclip15.ogv
	-wget -nc https://ia600204.us.archive.org/4/items/HSF-mov-apollo11_onbclip16/apollo11_onbclip16.ogv -O videos/onbclip16.ogv
	-wget -nc https://ia600204.us.archive.org/4/items/HSF-mov-apollo11_onbclip17/apollo11_onbclip17.ogv -O videos/onbclip17.ogv
