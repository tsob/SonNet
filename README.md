# SonNet
This project aims to sonify the inner workings of pretrained neural nets, for music, art and fun.

## Instructions

Once you take care of all the [dependencies (below)](#dependencies), you can
start the SuperCollider portion, [sonnet.rt.scd](sonnet.rt.scd). Then, launch
the Python portion, [sonnet.rt.py](sonnet.rt.py). (See options by typing
`python sonnet.rt.py -h`)

## Dependencies

I'm using Linux, but I assume things should run without too much trouble on
OSX.

### Python Virtual Environment
You'll probably want to set up a Python virtual environment
[(see, e.g., this site)][py-venv]. I use Python 2.7.

If you have [pip][pip], you should be able to install virtualenv with just

```bash
$ pip install virtualenv
```

After it's installed, you can create a new environment in this directory called
`venv` by executing:

```bash
$ virtualenv venv
```

After you've created this, you can activate it by

```bash
$ source ./venv/bin/activate
```

and when you're done, just

```bash
$ deactivate
```

### TensorFlow

We currently use [TensorFlow][tf] version 0.11. You can find instructions for
installing TensorFlow on your system, in a virtual environment,
[here][tf-install]. In my case, I can activate my virtual environment and then
run the following:

```bash
# Ubuntu/Linux 64-bit, CPU only, Python 2.7
(venv)$ export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-0.11.0rc0-cp27-none-linux_x86_64.whl
(venv)$ pip install --upgrade $TF_BINARY_URL
```

You should now be able to `import tensorflow` within your virtual environment!

### Keras

We use [Keras][keras] 1.1.0 ([github link][keras-git]). I install it with:

```bash
(venv)$ pip install --upgrade git+https://github.com/fchollet/keras.git
```

I also configure Keras by creating a file at `~/.keras/keras.json` (if it's not
already there) and making sure it has the following contents:
```json
{
  "image_dim_ordering": "tf",
  "epsilon": 1e-07,
  "floatx": "float32",
  "backend": "tensorflow"
}
```

### OpenCV and Linux

At least on Ubuntu and Fedora, installing OpenCV and its python bindings
`cv2` can be difficult. I did the following:

* Install OpenCV and bindings to your system (not virtual environment). For
Ubuntu, this was:

```bash
$ sudo apt-get install libopencv-dev python-opencv
```

For Fedora, something like [described here][cv2-fedora] should work:

```bash
$ sudo yum install opencv*
```

* Symlink `cv2` to your virtual environment's site packages.

```bash
$ cd /path/to/venv/lib/python2.7/site-packages/
# For Ubuntu:
$ ln -s /usr/lib/python2.7/dist-packages/cv2.x86_64-linux-gnu.so cv2.so
# For Fedora:
$ ln -s /usr/lib/python2.7/dist-packages/cv2.so cv2.so
```

Note that mine was called `cv2.x86_64-linux-gnu.so` on Ubuntu, but yours might
be different. Just search via `ls /usr/lib/python2.7/dist-packages/cv2*`.

### Additional packages

Finally, to make sure you have all other miscellaneous Python packages, you can execute

```bash
(venv)$ pip install -r requirements.txt
```

This will take all of the requirements listed in [requirements.txt](requirements.txt) and try to install them to your virtual environment.

### Nets

We use [François Chollet][fchollet]'s
[Deep Learning Models][deep-learning-models] to easily import pretrained models
(both their architectures and weights) in Keras. That repository is included
here as a submodule. You can make sure you have the latest by running

```bash
$ make update
```

This will make sure you update that repository, and add an empty `__init__.py`
file so that we can use it as a Python module.

### SuperCollider

We use [SuperCollider][sc] for some audio synthesis. You can
[download it here][sc-dl], it's cross-platform.


## Authors

* **Tim O'Brien** - [tsob](https://github.com/tsob)

## License

This project is licensed under the MIT License - see the
[LICENSE](LICENSE) file for details.


[py-venv]: http://docs.python-guide.org/en/latest/dev/virtualenvs/
[pip]: https://pip.pypa.io/en/stable/
[cv2-fedora]: https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_setup/py_setup_in_fedora/py_setup_in_fedora.html
[tf]: https://www.tensorflow.org/
[vgg-gist]: https://gist.github.com/baraldilorenzo/07d7802847aaad0a35d3
[vgg-weights]: https://drive.google.com/file/d/0Bz7KyqmuGsilT0J5dmRCM0ROVHc/view?usp=sharing
[tf-install]: https://www.tensorflow.org/versions/r0.11/get_started/os_setup.html#virtualenv-installation
[keras]: https://keras.io/
[keras-git]: https://github.com/fchollet/keras
[fchollet]: https://github.com/fchollet
[deep-learning-models]: https://github.com/fchollet/deep-learning-models
[sc]: http://supercollider.github.io/
[sc-dl]: http://supercollider.github.io/download.html
