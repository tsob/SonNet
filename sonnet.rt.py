#/usr/bin/env python2.7
"""
@name: sonnet.py
@desc: Sonifyiny Neural Nets with Python, Keras, Tensorflow, and more!
@auth: Tim O'Brien
@date: Octobre 5th, 2016
"""

import os
import copy
import numpy as np
import time
import argparse
import logging
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.image as mpimg
import seaborn as sns

import cv2

import OSC
from OSC import OSCClient, OSCMessage

import keras
from keras.preprocessing import image
from keras import backend as K
# from keras.utils.visualize_util import plot as keras_plot
from deep_learning_models.vgg16 import VGG16
from deep_learning_models.imagenet_utils import preprocess_input
from deep_learning_models.imagenet_utils import decode_predictions

# Supported image file extensions, for input
IMG_EXTENSIONS = ['bmp', 'eps', 'gif', 'icns', 'im', 'jpeg', 'jpg', 'msp',
                  'pcx', 'png', 'tiff', 'xbm', 'fpx', 'ico', 'psd', 'xpm']

VIDEO_EXTENSIONS = ['avi', 'mov', 'mp4', 'mkv']

IMG_TARGET_SIZE = (224, 224)

# OSC Stuff
SUPERCOLLIDER_PORT = 57121
CLIENT = OSCClient()
CLIENT.connect(("localhost", SUPERCOLLIDER_PORT))

# Plotting-related
sns.set_palette("dark")
sns.set_style("darkgrid")


def main(input_visual_path, frame_rate=None, outputdir=None):
    """
    Main function
    """

    logger = logging.getLogger(__name__)
    logger.info("Starting SonNet!")

    x, img, frame_rate = parse_input(input_visual_path, frame_rate)

    n_frames = x.shape[0]  # Number of images in our input

    # Construct the model
    model = VGG16(weights='imagenet')
    logger.debug("Loaded VGG-16 model")

    # Add function to extract late activations
    get_layer_output = K.function([model.layers[0].input],
                                  [model.layers[-2].output])

    # Get predictions and activations
    # preds = model.predict(x)
    activations = np.squeeze(np.asarray(get_layer_output([x])), axis=0)
    logger.debug("Computed activations, shape={}".format(activations.shape))

    # Plot
    fig = None
    for i in xrange(n_frames):
        fig = plot_frame(x[i], img[i], activations[i], fig=fig)
        send_sonification_message(activations[i])
        if frame_rate is None:
            plt.pause(0.000001)
            raw_input("Press Enter to continue...")
        else:
            plt.pause(1.0/float(frame_rate))

    # Tell the sonification to be quiet
    send_sonification_message(np.zeros_like(activations[0]))

    if outputdir is None:
        return activations
    else:
        save_data_to_file(outputdir, activations, frame_rate)

    # TODO: system call to ffmpeg or something for combining audio and video


def send_sonification_message(activations):
    """
    Sends the activations to SuperCollider for sonification.
    """
    msg = OSCMessage("/sonnet/activations")
    msg.append(activations)
    CLIENT.send(msg)


def plot_frame(x, img, activations=None, fig=None):
    """
    Plot the images and activations relating to a frame.
    """
    if fig is None:
        fig = plt.figure()

    gs = gridspec.GridSpec(4, 6)

    fig.clf()

    ax_img = plt.subplot(gs[:3,:3])
    plt.imshow(img, interpolation="nearest")
    plt.axis('off')
    plt.title("Image")

    ax_x = plt.subplot(gs[:3,3:])
    plt.imshow(x, interpolation="nearest")
    plt.axis('off')
    plt.title("Preprocessed")

    if activations is not None:
        ax_activations = plt.subplot(gs[3,:])
        n_activations = activations.shape[0]
        plt.plot(activations)
        plt.title("Penultimate Layer Activations")
        plt.xlabel("Neuron")
        plt.ylabel("Activation")
        plt.xlim([0, n_activations])

    fig.set_tight_layout(True)

    return fig


def save_data_to_file(outputdir, activations, frame_rate):
    """
    Write activations and config parameters to files
    """
    logger = logging.getLogger(__name__)
    time_string = time.ctime().replace(' ','_').replace(':','-')

    fname = os.path.join(
            outputdir,
            "{}_activations.csv".format(time_string)
            )
    np.savetxt(fname, activations, delimiter=',')
    logger.info("Wrote activations to {}".format(fname))

    # And configuration parameters
    fname = os.path.join(
            outputdir,
            "{}_config.csv".format(time_string)
            )
    with open(fname, 'w') as fh:
        fh.write("{},{},{}".format(
            activations.shape[0], activations.shape[1], frame_rate))
    logger.info("Wrote config to {}".format(fname))


def parse_input(input_visual_path, frame_rate=None):
    """
    Given the input path, parse it into a Keras-ready format.
    """
    logger = logging.getLogger(__name__)
    input_ext = os.path.splitext(input_visual_path)[1][1:].lower()

    # Photo input
    if (input_ext in IMG_EXTENSIONS) and ("%" not in input_visual_path):
        # Get the image input
        x, img = load_and_preprocess_image(input_visual_path)
        logger.debug("Loaded input from {}".format(input_visual_path))

    # Video input
    elif (input_ext in VIDEO_EXTENSIONS) or \
            ((input_ext in IMG_EXTENSIONS) and ("%" in input_visual_path)):

        if frame_rate is not None:
            logger.warning(
                    "A frame rate is specified, but we'll get that from the "
                    + "video itself. Input frame rate will be discarded.")

        # Get the video input
        x, img, frame_rate = load_and_preprocess_video(input_visual_path)

    # Unrecognized input
    else:
        e = NotImplementedError(
            "{} is not a supported filetype".format(input_ext.upper()))
        logging.exception(e)
        raise e

    return x, img, frame_rate


def load_and_preprocess_image(img_path='images/cat.jpg'):
    """
    Given a path to an image file, load it and preprocess it.

    Output is x, img, where x is the preprocessed image and img is the resized
    image. Both have dimensions (1, width, height, 3), where axis 0 corresponds
    to just 1 image in the batch, and axis 3 corresponds to 3 image color
    channels (RGB).
    """
    input_img = image.load_img(img_path, target_size=IMG_TARGET_SIZE)
    input_img = image.img_to_array(input_img)
    input_img = np.expand_dims(input_img, axis=0)
    x = preprocess_input(input_img)

    img = mpimg.imread(img_path)

    return x, np.expand_dims(img, axis=0)


def load_and_preprocess_video(video_path):
    """
    Load a video and preprocess each frame as successive images, for input to
    the net.
    """

    # Setup logger, as usual
    logger = logging.getLogger(__name__)
    logger.info("Loading and preprocessing video from {}".format(video_path))

    # Find OpenCV major version
    cv2_major_ver = (cv2.__version__).split('.')[0]

    # Open the video stream from file
    vidcap = cv2.VideoCapture(video_path)

    # Get frame rate, in frames per second
    if int(cv2_major_ver)  < 3 :
        frame_rate = vidcap.get(cv2.cv.CV_CAP_PROP_FPS)
    else :
        frame_rate = vidcap.get(cv2.CAP_PROP_FPS)
    logger.debug("Frame rate: {}".format(frame_rate))

    # Read the first frame
    success, vidframe = vidcap.read()
    input_shape = vidframe.shape
    logger.debug("Input frame shape: {}".format(input_shape))

    # Note that cv2 imports as BGR, not RGB. Change that.
    vidframe = cv2.cvtColor(vidframe, cv2.COLOR_BGR2RGB)
    img = np.asarray(copy.copy(vidframe))
    img = np.expand_dims(img, axis=0)

    # Resize
    vidframe = cv2.resize(vidframe, IMG_TARGET_SIZE,
                          interpolation=cv2.INTER_AREA)

    input_image = np.empty(shape=(1,IMG_TARGET_SIZE[0], IMG_TARGET_SIZE[1], 3))
    input_image[0] = vidframe

    count = 0
    success = True
    while success and (count<200):
        success, vidframe = vidcap.read()
        count += 1
        # Resize
        if vidframe is not None:
            # Convert to RGB
            vidframe = cv2.cvtColor(vidframe, cv2.COLOR_BGR2RGB)
            print np.asarray(vidframe).shape
            img = np.append(
                    img,
                    np.expand_dims(np.asarray(copy.copy(vidframe)),axis=0),
                    axis=0
                    )
            # Resize
            vidframe = cv2.resize(vidframe, IMG_TARGET_SIZE)
            # Add first dim for batch
            vidframe = np.expand_dims(vidframe, axis=0)
            # Append to batch
            input_image = np.append(input_image, vidframe, axis=0)

    logger.debug("Loaded {} frames".format(count))

    # Preprocess, for input into the net
    x = preprocess_input(input_image)

    return x, img, frame_rate


def parse_logging_level(level_arg):
    """
    Set the logging level based on the verbosity argument.
    """
    if level_arg == 0:
        logging_level = logging.WARNING
    elif level_arg == 1:
        logging_level = logging.INFO
    elif level_arg == 2:
        logging_level = logging.DEBUG
    else:
        logging_level = logging.DEBUG

    return logging_level


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--inputs",
                        required=False,
                        default="images/cat.jpg",
                        help="Input visual material (e.g. a .jpg image, "
                             + "a video file, or even a sequence of numbered "
                             + "images like 'img_%%02d.jpg' for img_00.jpg, "
                             + "img_01.jpg, etc.)")
    parser.add_argument("-o", "--outputdir",
                        required=False,
                        default=None,
                        help="Output directory to save activations.")
    parser.add_argument("-r", "--framerate",
                        required=False,
                        default=None,
                        type=float,
                        help="Frame rate, in frames per second, for rendering"
                             + " photo sequences.")
    parser.add_argument("-v", "--verbosity",
                        action="count",
                        help="increase output verbosity, e.g. -vv for "
                             + "debug level.")
    args = parser.parse_args()

    # Setup logger
    logging_level = parse_logging_level(args.verbosity)
    logging.basicConfig(
            level=logging_level,
            format='[%(levelname)s] %(message)s',
            )

    main(args.inputs, frame_rate=args.framerate, outputdir=args.outputdir)
