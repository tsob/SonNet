# Sonification
Thoughts and ideas.

## So many parameters

* A neural net layer may have a huge number of parameters, relative to what we're used to in computer music, *e.g.* 2048 or 4096.
* A simple sonification regime is to map each activation value to the amplitude of a sinusoidal oscillator at a fixed frequency. This has several problems.
    * For a length-2048 vector, and a linear frequency spacing from 20 Hz to 20 kHz, the difference in frequency from one oscillator to its neighbor is about 9.75 Hz. For a logarithmic frequency mapping over the same range, the marginal difference in frequency is 5.84 [cents][cent]. Thus, the resulting sound would be extremely broadband and atonal unless the activation vector was quite sparse. However, sparsity in the activations of intermediate layers of a neural net is undesirable; pre-trained image nets that perform well will likely exhibit dense activations.
    * A program with something on the order of 2048 or 4096 sinusoidal oscillators may not be able to be computed in real-time on most devices, at least currently. It would be best to allow real-time capabilities, both for interesting applications as well as ease of iterative development of sonification regimes.
* However, each value in the large activation vector is, more or less, equally important. We don't want a few dimensions controlling the most perceptually important musical/auditory aspects, while most dimensions contribute little.
* Perhaps we can enforce several small groupings, *i.e.* have a manageable number of instruments with several equally-important parameters controlled by a corresponding range of values in the activation vector.

### Orchestra Technique
* Physical models of several orchestral instruments, most of which are controlled by layer activations.

### Tonal Technique
* Assume length-2048 activations.
* Midi notes 0 to 135, i.e. ~**79** total generators. (Pitches divided over a heptatonic scale.)
* **25** parameters to map to each generator (midi note).
    * Panning
    * Gain
    * Reverb mix (global reverb)
    * 4 Envelope parameters ([ADSR][adsr])
    * Duration (*i.e.* sustain time)
    * Onset delay
    * 4 Vibrato parameters (frequency, amplitude, delay, attack)
    * **Need to think of 12 more...**
* **73** additional global parameters:
    * Reverb parameters
    * **Need to think of many more...**
    

    


[cent]: https://en.wikipedia.org/wiki/Cent_(music)
[adsr]: https://en.wikipedia.org/wiki/Synthesizer#Attack_Decay_Sustain_Release_.28ADSR.29_envelope