"""
@name: experiment_activations.py
@desc: Manipulate and plot calculated activations.
@auth: Tim O'Brien
@date: Oct. 30, 2016
"""

import os
from distutils.dir_util import mkpath
import argparse
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns

import scipy
from scipy.signal import fftconvolve

def moving_average(a, n=3) :
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n


def plot_activations(activations_path, outputdir):
    # Load activations
    activations = np.genfromtxt(activations_path, delimiter=',').T
    plt.figure(figsize=(8.5, 11))

    plt.subplot(311)
    plt.imshow(activations, aspect='auto', cmap='jet',
               interpolation='nearest', origin='lower'), plt.grid(False),
    plt.colorbar(label="Activation magnitudes")
    # plt.plot(np.argmax(activations, axis=0), 'wo')
    plt.axis('tight')
    plt.xlabel("Frame")
    plt.ylabel("Activation index")
    plt.title("Activations")

    plt.subplot(312)

    kernel_width = 15
    # kernel = np.ones((activations.shape[0], kernel_width)) * \
             # np.logspace(-4.0, 0.0, kernel_width)[np.newaxis,:]
    kernel = np.logspace(-4.0, 0.0, kernel_width)[np.newaxis, :]
    kernel = kernel / np.sum(kernel)
    activations_convolved = fftconvolve(activations, kernel, mode='same')

    plt.imshow(activations_convolved, aspect='auto', cmap='jet',
               interpolation='nearest', origin='lower'), plt.grid(False),
    plt.colorbar(label="Activation magnitudes")
    # plt.plot(np.argmax(activations, axis=0), 'wo')
    plt.axis('tight')
    plt.xlabel("Frame")
    plt.ylabel("Activation index")
    plt.title("Convolved activations")

    plt.subplot(313)

    act_over_convolved = activations / np.maximum(activations_convolved, 0.01)

    plt.imshow(act_over_convolved, aspect='auto', cmap='jet',
               interpolation='nearest', origin='lower'), plt.grid(False),
    plt.colorbar(label="Activation magnitudes")
    # plt.plot(np.argmax(activations, axis=0), 'wo')
    plt.axis('tight')
    plt.xlabel("Frame")
    plt.ylabel("Activation index")
    plt.title("Activations divided by convolved activations")

    output_path = os.path.join(
                    outputdir,
                    os.path.basename(activations_path[:-4] + '.pdf')
                    )
    plt.savefig(output_path, bbox_inches='tight')
    print("See plot saved as {}".format(output_path))


if __name__ == '__main__':
    # Commandline argument handling
    parser = argparse.ArgumentParser(
            description="SonNet, non-real-time implementation."
            )
    parser.add_argument("-i", "--input_activations",
                        required=False,
                        default="outputs/activations/sonnet_nrt.py." + \
                                "Thu_Oct_27_19-41-54_2016.activations.csv",
                        help="Path to input activations (csv file)")
    parser.add_argument("-o", "--outputdir",
                        required=False,
                        default='outputs/activation_plots',
                        help="Main output directory to save plots.")
    args = parser.parse_args()

    mkpath(args.outputdir)  # Create the output directory if necessary

    plot_activations(args.input_activations, args.outputdir)
